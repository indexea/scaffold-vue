import Vue from 'vue'
import VueI18n from 'vue-i18n'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import { Indexea } from './openapi.ts'
import Index from './components/index.vue'
import Error from './components/error.vue'
import config from '../config.json'
import en from './locales/en.json'
import cn from './locales/zh-CN.json'

const indexea = new Indexea(config.api, '', config.widget)

Vue.use(VueI18n)

const locale = /zh-CN/i.test(window.navigator.language) ? 'cn' : 'en'
const messages = { cn, en }
const current = messages[locale]
const i18n = new VueI18n({
  locale: locale,
  fallbackLocale: 'cn',
  messages: messages
})

/**
 * 读取 widget 详细信息并进行 vue 组件初始化
 */
indexea
  .widget()
  .then(widget => {
    document.title = widget.name
    if (widget.intro)
      document
        .querySelector('meta[name=description]')
        .setAttribute('content', widget.intro)

    new Vue({
      i18n,
      render: h =>
        h(Index, {
          props: { indexea, widget }
        })
    }).$mount('#IndexeaApp')
  })
  .catch(e =>
    new Vue({
      i18n,
      render: h =>
        h(Error, {
          props: { msg: e.message }
        })
    }).$mount('#IndexeaApp')
  )
